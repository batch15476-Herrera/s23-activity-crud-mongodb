
//add atleast 5 members of favorite fictional band
db.users.insertMany([
    {
        "firstName": "Lemon",
        "lastName": "Jang",
        "email": "janglemon@gmail.com",
        "password": "Lemon123",
        "isAdmin": false
    },
    {
        "firstName": "Pong",
        "lastName": "Markartley",
        "email": "Pong123@gmail.com",
        "password": "Pong123",
        "isAdmin": false
    },
    {
        "firstName": "Gorj",
        "lastName": "Haleyson",
        "email": "GH123@gmail.com",
        "password": "Gorj123",
        "isAdmin": false
    },
    {
        "firstName": "Starsy",
        "lastName": "Rich",
        "email": "Rich123@gmail.com",
        "password": "Rich123",
        "isAdmin": false
    },
    {
        "firstName": "Pete",
        "lastName": "Worst",
        "email": "Pete123@gmail.com",
        "password": "Pete123",
        "isAdmin": false
    }
])
//add 3 new courses in a new courses collection
db.courses.insertMany([
    {
        "name": "The boy band",
        "price": 2500,
        "isActive": false
    },
    {
        "name": "The man band",
        "price": 3000,
        "isActive": false
    },
    {
        "name": "The girl band",
        "price": 4000,
        "isActive": false
    }
])

//find all regular/non-admin users

db.users.find({})

//update the first user in the users collection as an admin

db.users.updateOne({"isAdmin": false},{$set:{"isAdmin": true}})

//update one of the courses as active

db.courses.updateOne({"isActive": false},{$set:{"isActive": true}})

//delete all inactive courses
db.courses.deleteMany({"isActive": false})